//
//  ViewController.m
//  gameTableDemo
//
//  Created by Ranosys on 7/14/16.
//  Copyright © 2016 Ranosys. All rights reserved.
//

#import "ViewController.h"
#import "secondViewController.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UISearchDisplayDelegate,  UISearchControllerDelegate>

@property (strong, nonatomic) IBOutlet UINavigationItem *myNavigationItem;
@property (strong, nonatomic) IBOutlet UITableView *myTableView;
@property (strong, nonatomic) NSMutableArray * gamesDetails;
@property (strong, nonatomic) NSMutableArray * gameImages;
@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) NSMutableArray *searchResult;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSMutableArray * badminton = [NSMutableArray arrayWithObjects:@"BadmintonA",@"badmintonB", nil];
    NSMutableArray * cricket = [NSMutableArray arrayWithObjects:@"cricketA", @"cricketB", nil];
    NSMutableArray * football = [NSMutableArray arrayWithObjects:@"footballA", @"footballB", nil];
    NSMutableArray * hockey = [NSMutableArray arrayWithObjects:@"hockeyA", @"hockeyB", nil];
    NSMutableArray * tennis = [NSMutableArray arrayWithObjects:@"tennisA", @"tennisB", nil];
    _gameImages = [NSMutableArray arrayWithObjects:badminton, cricket, football, hockey, tennis, nil];
    self.searchResult = [NSMutableArray arrayWithCapacity:[self.gamesDetails count]];
    
    NSMutableDictionary *game1 = [NSMutableDictionary dictionary];
    [game1 setObject:@"BADMINTON" forKey:@"NAME"];
    [game1 setObject:@"Badminton is a racquet sport played using racquets to hit a shuttlecock across a net. Although it may be played with larger teams, the most common forms of the game are singles (with one player per side) and doubles (with two players per side)." forKey: @"DETAIL"];
    [game1 setObject:@"badminton" forKey: @"IMAGE"];
    [game1 setObject:badminton forKey:@"IMAGEARRAY"];
    NSMutableDictionary *game2 = [NSMutableDictionary dictionary];
    [game2 setObject:@"CRICKET" forKey:@"NAME"];
    [game2 setObject:@"Cricket is a bat-and-ball game played between two teams of eleven players on a cricket field, at the centre of which is a rectangular 22-yard-long pitch with a wicket, a set of three wooden stumps sited at each end." forKey: @"DETAIL"];
    [game2 setObject:@"cricket" forKey: @"IMAGE"];
    [game2 setObject:cricket forKey:@"IMAGEARRAY"];
    NSMutableDictionary *game3 = [NSMutableDictionary dictionary];
    [game3 setObject:@"FOOTBALL" forKey:@"NAME"];
    [game3 setObject:@"Football is a family of team sports that involve, to varying degrees, kicking a ball to score a goal. Unqualified, the word football is understood to refer to whichever form of football is the most popular in the regional context in which the word appears." forKey: @"DETAIL"];
    [game3 setObject:@"football" forKey: @"IMAGE"];
    [game3 setObject:football forKey:@"IMAGEARRAY"];
    NSMutableDictionary *game4 = [NSMutableDictionary dictionary];
    [game4 setObject:@"HOCKEY" forKey:@"NAME"];
    [game4 setObject:@"Hockey is a family of sports in which two teams play against each other by trying to maneuver a ball or a puck into the opponent's goal using a hockey stick. In many areas, one sport (typically field hockey or ice hockey[1]) is generally referred to simply as hockey." forKey: @"DETAIL"];
    [game4 setObject:@"hockey" forKey: @"IMAGE"];
    [game4 setObject:hockey forKey:@"IMAGEARRAY"];
    NSMutableDictionary *game5 = [NSMutableDictionary dictionary];
    [game5 setObject:@"TENNIS" forKey:@"NAME"];
    [game5 setObject:@"Tennis is a racket sport that can be played individually against a single opponent (singles) or between two teams of two players each (doubles). Each player uses a tennis racket that is strung with cord to strike a hollow rubber ball covered with felt over or around a net and into the opponent's court.       Tennis is a racket sport that can be played individually against a single opponent (singles) or between two teams of two players each (doubles). Each player uses a tennis racket that is strung with cord to strike a hollow rubber ball covered with felt over or around a net and into the opponent's court. Tennis is a racket sport that can be played individually against a single opponent (singles) or between two teams of two players each (doubles). Each player uses a tennis racket that is strung with cord to strike a hollow rubber ball covered with felt over or around a net and into the opponent's court.Tennis is a racket sport that can be played individually against a single opponent (singles) or between two teams of two players each (doubles). Each player uses a tennis racket that is strung with cord to strike a hollow rubber ball covered with felt over or around a net and into the opponent's court." forKey: @"DETAIL"];
    [game5 setObject:@"tennis" forKey: @"IMAGE"];
    [game5 setObject:tennis forKey:@"IMAGEARRAY"];
    _gamesDetails = [NSMutableArray arrayWithObjects:game1,game2, game3, game4, game5, nil];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
//    [self.searchResult removeAllObjects];
    NSString * filter = @"%K CONTAINS[c] %@";
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:filter,@"NAME", searchText];
    self.searchResult = (NSMutableArray *)[_gamesDetails filteredArrayUsingPredicate:resultPredicate];

    [_myTableView reloadData];
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}
//-(BOOL) searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption
//{
//    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
//     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
//
//    return YES;
//}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return [self.searchResult count];
    }
    else
    {
        return _gamesDetails.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"cell";
    UITableViewCell *cell ;
    //    if (cell == nil)
    //    {
    //        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //    }
    
    cell = [self.myTableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    UIImageView *imgView  = (UIImageView *)[cell.contentView viewWithTag:1];
    UILabel * title = (UILabel *)[cell.contentView viewWithTag:2];
    UILabel * desc = (UILabel *)[cell.contentView viewWithTag:3];
    if(tableView == _myTableView){
    imgView.image = [UIImage imageNamed:_gamesDetails[indexPath.row][@"IMAGE"]];
    title.text = _gamesDetails[indexPath.row][@"NAME"];
    desc.text = _gamesDetails[indexPath.row][@"DETAIL"];
    }
    else
    {
        imgView.image = [UIImage imageNamed:_searchResult[indexPath.row][@"IMAGE"]];
        title.text = _searchResult[indexPath.row][@"NAME"];
        desc.text = _searchResult[indexPath.row][@"DETAIL"];
        self.searchDisplayController.searchResultsTableView.rowHeight = _myTableView.rowHeight;

    }
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   secondViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"svc"];
//    secondViewController *svc = [[secondViewController alloc] init];
    if( tableView == _myTableView){
    svc.gameTitle = _gamesDetails [indexPath.row][@"NAME"];
    svc.gameDesc =  _gamesDetails[indexPath.row][@"DETAIL"];
    UIImage * image = [UIImage imageNamed:_gamesDetails[indexPath.row][@"IMAGE"]];
    svc.gameImage = image ;
    NSMutableArray * detailImage = _gamesDetails[indexPath.row][@"IMAGEARRAY"] ;
    svc.gamePics = detailImage;
    }
    else{
        svc.gameTitle = _searchResult [indexPath.row][@"NAME"];
        svc.gameDesc = _searchResult [indexPath.row][@"DETAIL"];
        UIImage * image = [UIImage imageNamed:_searchResult[indexPath.row][@"IMAGE"]];
        svc.gameImage = image ;
        NSMutableArray * detailImage = _searchResult[indexPath.row][@"IMAGEARRAY"] ;
        svc.gamePics = detailImage;

    }
    [self.navigationController pushViewController:svc animated:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
